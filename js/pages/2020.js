"use strict"

const Beez = (function() {
	const data = {

	};

	const events = {

	};

	const methods = {
	  


	};

	const initialize = function() {
	 

	};

	return {
	  	init: initialize
	};
})();


document.addEventListener(
	'DOMContentLoaded',
	function() {
		Beez.init();
		
       
        const fila = document.querySelector(".carousel");
		const carousel_contenido = document.querySelector(".carousel_contenido");
		const flechaIzquierda = document.getElementById("flecha_izquierda");
		const flechaDerecha = document.getElementById("flecha_derecha");

		flechaDerecha.addEventListener("click", () => {
			fila.scrollLeft += fila.offsetWidth;
		});

		flechaIzquierda.addEventListener("click", () => {
			fila.scrollLeft -= fila.offsetWidth;
		});

	},
	false
);

